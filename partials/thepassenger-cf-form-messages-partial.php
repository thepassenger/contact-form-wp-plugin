<?php
$dbName = DB_NAME;
$columns = $wpdb->get_results("SELECT `COLUMN_NAME` 
FROM `INFORMATION_SCHEMA`.`COLUMNS` 
WHERE `TABLE_SCHEMA`='$dbName' 
    AND `TABLE_NAME`='$tableName';");

$onlyNotRead = (isset($_GET['showAll']) && abs($_GET['showAll']) === 1) ? "" : "where viewed = 0";

if (isset($_GET["orderby"]) && isset($_GET["order"])){

    $orderBy = sanitize_text_field($_GET["orderby"]);
    $order = sanitize_text_field($_GET["order"]);

    if (preg_match('/\s/', $order)) wp_die();

    $messages = $wpdb->get_results("select * from $tableName $onlyNotRead order by $orderBy $order", 'ARRAY_A');

} else {
    $messages = $wpdb->get_results("select * from $tableName $onlyNotRead", 'ARRAY_A');
};
//var_dump($wpdb->last_query);
$columns = array_combine($wpdb->get_col_info(), prettifyColumnNames($wpdb->get_col_info()));

$myListTable = new TP_MessagesTable($messages, $columns, $formObj->shortcode, $tableName);
$myListTable->prepare_items();


?>
<div class="wp_form_messages" data-formname="<?= $formObj->name ?>">
    <h2>Form: <?= $formObj->name ?></h2>
    <form id="events-filter" method="get">
        <input type="hidden" name="page" value="<?=  $_REQUEST['page'] ?>" />
        <input type="hidden" name="tp_form" value="<?=  $formObj->shortcode; ?>" />
        <?php
        $myListTable->display();
        ?>
    </form>
</div>
<?php
//die(var_dump($_GET));
//die(var_dump(wp_verify_nonce( $_GET["_wpnonce"])));
//

// Redirect after deleting. Not great but don't know other way to do it. Wp redirect throws errors.
if (isset($_GET["tp_form"]) && isset($_GET["action"]) && isset($_GET["_wpnonce"]) && ($_GET["action"] === 'viewed' || $_GET["action"] === 'deleted')) {
    echo "<script>window.location.href = document.referrer</script>";
}

