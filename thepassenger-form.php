<?php
/*
Plugin Name: ThePassenger Contact Form
Plugin URI: http://example.com
Description: Simple dynamic Wordpress Form with email and store in database functionality.
Version: 1.0
Author: Pulioz
Author URI: Pulioz
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define( 'THEPASSENGER_FORM__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

add_shortcode('thepassenger_contactform', array('ThePassengerForm', 'addShortCode'));

require_once ('class.thepassenger-form.php');

/**
 * Create the thepassenger_forms table in the database.
 *
 * This table will contain all relevant details about each form you create.
 *
 **/
function thepassenger_create_forms_table() {
    global $wpdb;

    $table_name = $wpdb->prefix . 'thepassenger_forms';
    $charset_collate = $wpdb->get_charset_collate();
    try {
        $sql = "CREATE TABLE $table_name (
            id mediumint NOT NULL AUTO_INCREMENT,
            name varchar(100) NOT NULL UNIQUE,
            shortcode varchar(100) NOT NULL UNIQUE,
            structure blob NOT NULL,            
            html text NOT NULL,
            created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,            
            PRIMARY KEY  (id)
            ) $charset_collate;";
    } catch (Exception $e){
        echo 'There was a problem creating the database messages table.';
    };  

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

register_activation_hook( __FILE__, 'thepassenger_create_forms_table' );
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
	require_once( THEPASSENGER_FORM__PLUGIN_DIR . 'admin/class.thepassenger-form-admin.php' );
	add_action( 'init', array( 'ThePassengerFormAdmin', 'init' ) );
}

?>