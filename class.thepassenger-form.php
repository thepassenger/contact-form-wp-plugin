<?php

class ThePassengerForm {

    protected $data;
    protected $tableName;


    public function __construct($data, $tableName)
    {
        $this->data = $data;
        $this->tableName = $tableName;
    }

    public function store()
    {
        global $wpdb;

        $wpdb->insert($wpdb->prefix . $this->tableName . '_messages', $this->data );
        if ($wpdb->last_error) {
            throw new Exception($wpdb->last_error);
        }
    }

    /**
     * Generate the form html from an object data.
     *
     * @param array $data The data associative array.
     * @return string The html output.
     **/
    public static function html($data)
    {
        require_once(dirname(__FILE__) . '/thepassenger-helpers.php');
        
        $fieldsHtml = generateFieldsHtml($data);
        
        return $html = '<form method="post" action="#">
            <input type="hidden" name="tp_form_submitted">' . $fieldsHtml . '</form>';
    }

    /**
     * Check if the 
     *
     *
     *
     */
    public static function isValid($data)
    {
        foreach($data as $input) {
            if (! $input) return false;
        }
        return true;
    }

    public function sendEmail()
    {
        $to = get_option("admin_email");
        $subject = "New Message from {$this->tableName} form.";
        ob_start();
        require_once( THEPASSENGER_FORM__PLUGIN_DIR . 'templates/email.php');
//        require 'otherfile.php';
        $message = ob_get_clean();
//        $message = emailTemplate($this->data);
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail($to, $subject, $message, $headers);
    }

    public static function addShortCode($attrs)
    {
        global $wpdb;
        shortcode_atts( array(
            'name' => '',
        ), $attrs, 'thepassenger_contactform' );
        $tableName = $attrs['name'];
        $form = $wpdb->get_results("select * from " . $wpdb->prefix . "thepassenger_forms where shortcode='{$tableName}'");
        
        self::submit($tableName);
        return $form[0]->html;
       
    }

    public static function submit($tableName)
    {
        if (isset($_POST['tp_form_submitted'])){   
            $data = $_POST;
            unset($data['tp_form_submitted']); 
            $sanitizedData = self::sanitize($data);
            $form = new ThePassengerForm($sanitizedData, $tableName);
            try {
                $form->store();
                echo '<div>';
                echo '<p>Thanks for contacting me, expect a response soon.</p>';
                echo '</div>';
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            
            try {
                $form->sendEmail();
            }
            catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * Sanitize the user input
     *
     * @param array $data Associative array with every input name and its value.
     * @return array $sanitizedData Associative array with same input name but value is sanitized.
     *
     */
    public static function sanitize($data)
    {
        $sanitizedData = [];
        foreach($data as $key => $value) {
            if (preg_match('/^\s*[^-\s]*@.*\.[^-\s]*\s*$/', $value)) {
                $sanitizedData = array_merge($sanitizedData, [$key => sanitize_email( $value )]);
            } else {
                if (is_array($value)) {
                    $sanitizedValue = '';
                    foreach($value as $elem) {
                        $sanitizedValue .= sanitize_text_field($elem) . ',';
                    };
                    $sanitizedData = array_merge($sanitizedData, [$key => substr($sanitizedValue, 0, -1)]);                    
                } else {
                    $sanitizedData = array_merge($sanitizedData, [$key => sanitize_text_field( $value )]);
                }
            }
        };

        return $sanitizedData;
    }

    /**
     * Creates the database wp_{form_name}_messages to store the submitted messages.
     *
     * @param mixed[] $data Array that contains the submitted $_POST['form'] object.
     * @param string $tableName String with the form shortcode to generate the table name.
     */
    public static function createMessagesTable($data, $shortcode)
    {
        global $wpdb;

        $table_name = $wpdb->prefix . $shortcode . '_messages';
        $charset_collate = $wpdb->get_charset_collate();
        
        $columns = self::generateSqlTypes($data);
        try {
            $sql = "CREATE TABLE $table_name (
                id mediumint NOT NULL AUTO_INCREMENT,
                {$columns}
                created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                viewed BOOLEAN DEFAULT 0,
                PRIMARY KEY  (id)
                ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        } catch (Exception $e){
            echo 'There was a problem creating the database messages table.';
        };
    }

    /**
    * Generate Sql column types from html input types
    *
    * @param array[] $inputsAttr - An array of arrays containing the field attributes.
    * @return string $sql String that cointains the sql to create the columns based on input.
    */
    public static function generateSqlTypes($data)
    {

        $sql = '';
        $addedNames = [];
        
        foreach(array_filter($data['fields'], function($field){ 
            return array_key_exists('name', $field['attrs']) && $field['attrs']['name'];
        }) as $field)  {
            switch ($field['type'])  {
                case 'text':
                case 'url':                
                    $required = $field['attrs']['required'] ? 'NOT NULL' : "DEFAULT NULL";
                    $sql .= stripSpace($field['attrs']['name']) . " text {$required},";
                    break;
                case 'email':
                case 'password':
                case 'hidden':
                case 'color':    
                case 'month':
                    $required = $field['attrs']['required'] ? 'NOT NULL' : "DEFAULT NULL";
                    $sql .= stripSpace($field['attrs']['name']) . " VARCHAR(255) {$required},";
                    break;
                case 'checkbox':
                case 'radio':
                    if (in_array($field['attrs']['name'], $addedNames)){
                        break;
                    } else {
                        array_push($addedNames, $field['attrs']['name']);
                        $required = $field['attrs']['required'] ? 'NOT NULL' : "DEFAULT NULL";
                        $sql .= stripSpace($field['attrs']['name']) . " VARCHAR(255) {$required},";
                        break;
                    };                    
                case 'date':
                    $required = $field['attrs']['required'] ? 'NOT NULL' : "DEFAULT NULL";
                    $sql .= stripSpace($field['attrs']['name']) . " DATE {$required},";
                    break;    
                case 'datetime':
                    $required = $field['attrs']['required'] ? 'NOT NULL' : "DEFAULT NULL";
                    $sql .= stripSpace($field['attrs']['name']) . " TIMESTAMP {$required},";
                    break;
                case 'number':
                    $required = $field['attrs']['required'] ? 'NOT NULL' : "DEFAULT NULL";
                    $sql .= stripSpace($field['attrs']['name']) . " INT {$required},";
                    break;
                default:
                    if ($field['tag'] === 'textarea') {
                        $required = $field['attrs']['required'] ? 'NOT NULL' : "DEFAULT NULL";
                        $sql .= stripSpace($field['attrs']['name']) . " text {$required},";
                    }
                    break;
            }
        }
        return $sql;
    }
}