<?php

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

global $wpdb;

class My_List_Table extends WP_List_Table
{

    public $forms;

    /**
     * My_List_Table constructor.
     * @param $forms
     */
    public function __construct($forms)
    {
        parent::__construct(array(
            'singular' => 'form',
            'plural' => 'forms',
            'ajax' => false
        ));

        $this->forms = $forms;
    }

    // Display checkboxes
    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            //$this->_args['singular'],
            $this->_args['singular'],
            $item["id"]
        );
    }

    function get_columns(){
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'name' => 'Name',
            'shortcode'    => 'Shortcode',
            'created_at'      => 'Created At'
        );
        return $columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => __( 'Delete Form Only' , 'visual-form-builder'),
            'deleteAndDrop' => __( 'Delete Form and Messages' , 'visual-form-builder')
        );

        return $actions;
    }

    function process_bulk_action() {

//        $entry_id = ( is_array( $_REQUEST['entry'] ) ) ? $_REQUEST['entry'] : array( $_REQUEST['entry'] );
        switch ($this->current_action()) {
            case 'delete':
                self::thepassenger_delete_form();
                break;
            case 'deleteAndDrop':
                self::thepassenger_delete_form(true);
                break;
            default:
                break;
//        if ( 'delete' === $this->current_action() ) {
//
//
//            global $wpdb;
////
//            wp_verify_nonce( $_GET["_wpnonce"] );
//            foreach ( $_GET["form"] as $formId ) {
//
////                $id = absint( $formId );
////                $table = $wpdb->prefix . "thepassenger_forms";
////                $wpdb->query( "DELETE FROM $table WHERE id = $id" );
//
////                exit;
//            }
//            wp_redirect($_SERVER["HTTP_REFERER"]);
//            exit();


        }
    }

    public static function thepassenger_delete_form($deleteMessages = false)
    {
        global $wpdb;

        $shortcodes = $wpdb->get_results("select id,shortcode from " . $wpdb->prefix . "thepassenger_forms");
//        wp_die(var_dump(array_filter($shortcodes, function($item){
//            return $item->id === '53';
//        })));


        wp_verify_nonce( $_GET["_wpnonce"] );
        foreach ( $_GET["form"] as $formId ) {
            $id = absint( $formId );
            $table = $wpdb->prefix . "thepassenger_forms";

            if ($deleteMessages) {

                $shortcode = array_values(array_filter($shortcodes, function($item) use ($formId) {
                    return $item->id === $formId;
                }))[0]->shortcode;

                if (preg_match('/\s/', $shortcode)) wp_die();

                $sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . $shortcode . "_messages;";

                $wpdb->query($sql);
            }

            $wpdb->query( "DELETE FROM $table WHERE id = $id" );

        }
    }

    function prepare_items() {

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array('name' => array('name',false), 'created_at' => array('created_at',false));
        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $this->items = $this->forms;

//        die(var_dump($columns));
    }

    function column_default( $item, $column_name ) {
        switch( $column_name ) {
            case 'name':
//                die(var_dump($item));
                return "<a href='" . esc_url($_SERVER["PHP_SELF"]) . "?page=thepassenger-cf-admin-edit&form=" . $item["id"] . "&action=edit'>"  . $item[ $column_name ] . "</a>
                        <div class=\"row-actions\">
                            <span>
                                <a href='" . esc_url($_SERVER["PHP_SELF"]) . "?page=thepassenger-cf-admin-edit&form=" . $item["id"] . "&action=edit'>Edit</a> |
                            </span>
                            <span >
                                <a href='" . esc_url($_SERVER["PHP_SELF"]) . "?page=thepassenger-cf-admin-edit&form=" . $item["id"] . "&action=duplicate'>Duplicate</a> |
                            </span>
                            <span >
                                <a href='" . esc_url($_SERVER["PHP_SELF"]) . "?page=thepassenger-cf-admin-edit&form=" . $item["id"] . "&action=duplicateWithMessages'>Duplicate With Messages</a> 
                            </span>
                        </div>";
            case 'shortcode':
            case 'created_at':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
        }
    }

}


if (isset($_GET["orderby"]) && isset($_GET["order"])){

    $orderBy = sanitize_text_field($_GET["orderby"]);
    $order = sanitize_text_field($_GET["order"]);

    if (preg_match('/\s/', $order)) wp_die();

    switch ($orderBy) {
        case "name":
            $forms = $wpdb->get_results("select * from " . $wpdb->prefix . "thepassenger_forms order by $orderBy $order", 'ARRAY_A');
            break;
        case "created_at":
            $forms = $wpdb->get_results("select * from " . $wpdb->prefix . "thepassenger_forms order by $orderBy $order", 'ARRAY_A');
            break;
        default:
            $forms = $wpdb->get_results("select * from " . $wpdb->prefix . "thepassenger_forms", 'ARRAY_A');
            break;
    };
} else {
    $forms = $wpdb->get_results("select * from " . $wpdb->prefix . "thepassenger_forms", 'ARRAY_A');
};

$myListTable = new My_List_Table($forms);
$myListTable->prepare_items();

//add_filter( 'bulk_actions-page', 'my_bulk_action_handler', 10, 3 );
//
//function my_bulk_action_handler( $redirect_to, $doaction, $post_ids ) {
//    wp_die('qua');
//    if ( $doaction !== 'email_to_eric' ) {
//        return $redirect_to;
//    }
//    foreach ( $post_ids as $post_id ) {
//        // Perform action for each post.
//    }
//    $redirect_to = add_query_arg( 'bulk_emailed_posts', count( $post_ids ), $redirect_to );
//    return $redirect_to;
//}

?>
<form id="events-filter" method="get">
    <input type="hidden" name="page" value="<?=  $_REQUEST['page'] ?>" />
    <?php
    $myListTable->display();
    ?>
</form>

<?php
//die(var_dump($_GET));
//die(var_dump(wp_verify_nonce( $_GET["_wpnonce"])));
//

// Redirect after deleting. Not great but don't know other way to do it. Wp redirect throws errors.
if (isset($_GET["form"]) && isset($_GET["action2"]) && isset($_GET["_wpnonce"]) && ($_GET["action2"] === 'delete' || $_GET["action2"] === 'deleteAndDrop')) {
    echo "<script>window.location.href = document.referrer</script>";
}



