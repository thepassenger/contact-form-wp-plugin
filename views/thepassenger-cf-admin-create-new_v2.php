<?php
global $wpdb;

if (isset($_GET['form'])) {
    $form = $wpdb->get_results("select * from " . $wpdb->prefix . "thepassenger_forms where id = {$_GET['form']}")[0];
    if (!$form) {
        die('Wrong turn');
    }
}
?>

<h1>Create a new form here.</h1>

<br><br>
<div class="generator-list">
    <a href="#" id="text-generator" class="generator" data-type="text">Text</a>
    <a href="#" id="email-generator" class="generator" data-type="email">Email</a>
    <a href="#" id="password-generator" class="generator" data-type="password">Password</a>
    <a href="#" id="radio-generator" class="generator" data-type="radio">Radio Button</a>
    <a href="#" id="checkbox-generator" class="generator" data-type="checkbox">Checkbox</a>
    <a href="#" id="button-generator" class="generator" data-type="button">Button</a>
    <a href="#" id="color-generator" class="generator" data-type="color">Color</a>
    <a href="#" id="date-generator" class="generator" data-type="date">Date</a>
    <a href="#" id="datetime-local-generator" class="generator" data-type="datetimelocal">Datetime-local</a>
    <a href="#" id="month-generator" class="generator" data-type="month">Month</a>
    <a href="#" id="url-generator" class="generator" data-type="url">Url</a>
    <a href="#" id="textArea-generator" class="generator" data-type="textarea">Text Area</a>
    <a href="#" id="number-generator" class="generator" data-type="number">Number</a>
    <a href="#" id="hidden-generator" class="generator" data-type="hidden">Hidden</a>

</div>
<!-- The Modal -->
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <form action="<?= esc_url($_SERVER['REQUEST_URI']) ?>" method="post">
            <input type="hidden" name="test_field">
            <div class="modal-fields">
            </div>
        </form>
    </div>
</div>
<br>
<br>
<div id="add-fields">
</div>
<br>
<br>

<form id="new-form">
    <div id="new-form-header">
        <label for="thepassenger_form_new_name">Form Name</label>
        <input type="text" name="thepassenger_form_new_name" placeholder="Form name"
            <?= isset($form) ? "value=\"{$form->name}\"" : '' ?>>
        <?= isset($form) ? "<input type=\"hidden\" id=\"form-structure\" value={$form->structure}>" : '' ?>
        <button type="button" id="create-form-button">Create Form</button>
        <span id="success-message">
            The form has been created.
            Copy and paste this shortcode into your post, page or text widget content.
            [thepassenger_contactform name="<span></span>"]
        </span>
    </div>
    <p> Here it's how the form will look like.</p>
    <div class="fields">
    </div>
</form>
