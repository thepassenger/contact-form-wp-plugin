<h1> Here are your created forms </h1>
<?php

global $wpdb;

$forms = $wpdb->get_results("select * from " . $wpdb->prefix . "thepassenger_forms");
?>
<!-- The Modal -->
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">

        <form method="post">
            <input type='hidden' name='formId' value="">
            <input type='hidden' name='formShortCode' value="">
            <button type='button' class='delete-form-button'>Delete</button>
            <button type='button' class='close-modal-button'>Close</button>
        </form>
    </div>
</div>
<table>
    <tr>
        <th>
            Name
        </th>
        <th>
            Shortcode
        </th>
        <th>
            Created At
        </th>
    </tr>
    <?php
    foreach ($forms as $form) {
        echo '<tr>';
        foreach ($form as $key => $value) {
            switch ($key) {
                case 'name':
                    echo "<td><a href='" . esc_url($_SERVER['PHP_SELF']) . '?page=thepassenger-cf-admin-edit&form=' . $form->id . "'>" . $value . "</a></td>";
                    break;
                case 'shortcode':
                    echo "<td>[thepassenger_contactform name=\"$value\"]</td>";
                    break;
                case 'created_at':
                    echo "<td>" . $value . "</td>";
                    break;
                default:
                    break;
            }
        };

        echo "
                <td>
                    <button data-formName='" . $form->name . "' data-formId='" . $form->id . "' data-formShortCode='" . $form->shortcode . "' type='button' class='open-modal-button'>Delete</button>
                </td>";
        echo '</tr>';
    };
    ?>
</table>
