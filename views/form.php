<form action="<?= esc_url( $_SERVER['REQUEST_URI'] )?>" method="post">
        <p>
                Your Name (required) <br />
                <input type="text" name="cf-name" pattern="[a-zA-Z0-9 ]+" 
                        value="<?= isset( $_POST["cf-name"] ) ? esc_attr( $_POST["cf-name"] ) : '' ?>"
                        size="40" required>
        </p>
        <p>
                Your Email (required) <br />
                <input type="email" name="cf-email" 
                        value="<?=  isset( $_POST["cf-email"] ) ? esc_attr( $_POST["cf-email"] ) : ''  ?>"
                        size="40" required>
        </p>
        <p>
                Subject (required) <br />
                <input type="text" name="cf-subject" pattern="[a-zA-Z ]+" 
                        value="<?= isset( $_POST["cf-subject"] ) ? esc_attr( $_POST["cf-subject"] ) : ''  ?>"
                        size="40" required>
        </p>
        <p>
                Your Message (required) <br />
                <textarea rows="10" cols="35" name="cf-message" required><?=
                        isset( $_POST["cf-message"] ) ? esc_attr( $_POST["cf-message"] ) : '' 
                ?></textarea>
        </p>
        <p><input type="submit" name="cf-submitted" value="Send"/></p>
</form>