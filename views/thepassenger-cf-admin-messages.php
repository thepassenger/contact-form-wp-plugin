<div>
    <h1>Contact Form Messages</h1>
    <form action="<?= $_SERVER["PHP_SELF"] ?>" method="get">
        <input type="hidden" name="page" value="<?=  $_REQUEST['page'] ?>">
        <?php
        if (!((isset($_GET['showAll']) && abs($_GET['showAll']) === 1))) echo "
        <input type=\"hidden\" name=\"showAll\" value=\"1\">
        "; ?>
        <button type="submit" class="button"><?php echo ((isset($_GET['showAll']) && abs($_GET['showAll']) === 1)) ?  "Show Read Messages Only" :  "Show All Messages"; ?></button>
    </form>

    <input type="text" placeholder="filter" id="filter-messages-form">

<!--    <form action="--><?php//= $_SERVER["PHP_SELF"] ?><!--">-->
<!--        --><?php
//            foreach ($_GET as $key => $value) {
//                switch ($key) {
//                    case "page":
//                        echo '<input type="hidden" name="page" value="' . sanitize_text_field($value) . '">';
//                        break;
//                    case "showAll":
//                        echo '<input type="hidden" name="page" value="' . sanitize_text_field($value) . '">';
//                        break;
//
//                }
//            }
//        ?>
<!--    </form>-->
<!--    TODO Reset order by if we filter forms -->
    <?php
    global $wpdb;
    $shortcodes = $wpdb->get_results("select id, name, shortcode from " . $wpdb->prefix . "thepassenger_forms;");

    foreach ($shortcodes as $formObj) {
//            wp_die(var_dump($formObj));

        $tableName = $wpdb->prefix . $formObj->shortcode . "_messages";
        if ( ! $wpdb->get_results("SHOW TABLES LIKE '$tableName'")) continue;
//        echo "<option value=\"$key\">Form: $formObj->name </option>";
//        $messages = $wpdb->get_results("select * from " . $tableName . " WHERE viewed = 0;");
//        var_dump($messages);
//        $numMessages = count($messages);
//        $columns = array_map(function($dunno) {
//            if ( $dunno === 'id' || $dunno === 'viewed') return;
//            return $dunno;
//        },$wpdb->get_col_info());
//        $columns = array_diff($wpdb->get_col_info(), ["viewed"] );
        require(dirname(__FILE__) . '/../partials/thepassenger-cf-form-messages-partial.php');
    }
//    echo "			    </select>
//		</div>
//	</form>
//   ";
    ?>
</div>

<?php

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

global $wpdb;

class TP_MessagesTable extends WP_List_Table
{

    public $messages;
    public $columns;
    public $shortcode;
    public $tableName;

    /**
     * TP_MessagesTable constructor.
     * @param $messages
     */
    public function __construct($messages, $columns, $shortcode, $tableName)
    {
        parent::__construct(array(
            'singular' => 'message',
            'plural' => 'messages',
            'ajax' => false
        ));

        $this->messages = $messages;
        $this->columns = $columns;
        $this->shortcode = $shortcode;
        $this->tableName = $tableName;
    }

    // Display checkboxes
    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            //$this->_args['singular'],
            $this->_args['singular'],
            $item["id"]
        );
    }

    function get_columns(){
        $columns = array_merge(array(
            'cb' => '<input type="checkbox" />'
        ), $this->columns);
        return $columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'viewed' => __( 'Mark as Viewed' , 'visual-form-builder'),
            'deleted' => __( 'Delete' , 'visual-form-builder')
        );

        return $actions;
    }

    function process_bulk_action() {

//        $entry_id = ( is_array( $_REQUEST['entry'] ) ) ? $_REQUEST['entry'] : array( $_REQUEST['entry'] );
        switch ($this->current_action()) {
            case 'viewed':
                $this->thepassenger_mark_as_viewed();
                break;
            case 'deleted':
                $this->thepassenger_delete_messages();
                break;
            default:
                break;
        }
    }

    public function thepassenger_mark_as_viewed()
    {
        global $wpdb;

//        $shortcodes = $wpdb->get_results("select id,shortcode from " . $wpdb->prefix . "thepassenger_messages");
//        wp_die(var_dump(array_filter($shortcodes, function($item){
//            return $item->id === '53';
//        })));

        wp_verify_nonce( $_GET["_wpnonce"] );

        $shortcode = sanitize_text_field($_GET["tp_form"]);

        if ($this->shortcode !== $_GET['tp_form']) return;

        $messages = array_map(function($item){
            return abs($item);
        }, $_GET["message"]);

        $messageIds = implode(',', $messages);

//        $wpdb->update($wpdb->tableName, ['viewed' => 1], ['id' => $key]);
        $sql = "UPDATE $this->tableName SET viewed = 1 WHERE id in ($messageIds)";
        $wpdb->query($sql);
    }

    public function thepassenger_delete_messages()
    {
        global $wpdb;

        wp_verify_nonce( $_GET["_wpnonce"] );

        $shortcode = sanitize_text_field($_GET["tp_form"]);

        if ($this->shortcode !== $_GET['tp_form']) return;

        $messages = array_map(function($item){
            return abs($item);
        }, $_GET["message"]);

        $messageIds = implode(',', $messages);

        $wpdb->query( "DELETE FROM $this->tableName WHERE id in ($messageIds)" );

    }

    function prepare_items() {

        $columns = $this->get_columns();
        $sortable = $columns;
        unset($sortable["cb"]);
        unset($sortable["viewed"]);

        $hidden = array('viewed');

        // Make all columns sortable.
        $sortable = array_combine(array_keys($sortable), array_map(function($item){
            return array($item, false);
        },array_keys($sortable)));
        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $this->items = $this->messages;

//        die(var_dump($columns));
    }

    function column_default( $item, $column_name ) {
        switch( $column_name ) {
            case "id":
                return $item["id"] .
                        "<div class=\"row-actions\">
                            <span>
                                <a href='" . esc_url($_SERVER["REQUEST_URI"]) . "&tp_form=" . $this->shortcode . "&_wpnonce=" . wp_create_nonce() . "&action=viewed&message[]=" . $item["id"] . "'>Mark as Viewed</a> |
                            </span>
                            <span>
                                <a href='" . esc_url($_SERVER["REQUEST_URI"]) . "&tp_form=" . $this->shortcode . "&_wpnonce=" . wp_create_nonce() . "&action=deleted&message[]=" . $item["id"] . "'>Delete</a>
                            </span>
                        </div>";
            default:
                return $item[ $column_name ];
//                return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
        }
    }

}









