<?php
$data = []; ?>
<h1>Create a new form here.</h1>

<br><br>
<div class="generator-list">
    <a href="#" id="text-generator" class="generator">Text</a>
    <a href="#" id="email-generator" class="generator">Email</a>
    <a href="#" id="password-generator" class="generator">Password</a>
    <a href="#" id="radio-generator" class="generator">Radio Button</a>
    <a href="#" id="checkbox-generator" class="generator">Checkbox</a>
    <a href="#" id="button-generator" class="generator">Button</a>
    <a href="#" id="color-generator" class="generator">Color</a>
    <a href="#" id="date-generator" class="generator">Date</a>
    <a href="#" id="datetime-local-generator" class="generator">Datetime-local</a>
    <a href="#" id="month-generator" class="generator">Month</a>
    <a href="#" id="url-generator" class="generator">Url</a>
    <a href="#" id="textArea-generator" class="generator">Text Area</a>    
    <a href="#" id="number-generator" class="generator">Number</a>
    <a href="#" id="hidden-generator" class="generator">Hidden</a>

</div>
        <!-- The Modal -->
    <div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
    </div>
</div>
<br>
<br>
<div id="add-fields">
</div>
<br>
<br>

<form id="new-form" method="post" action="<?= esc_url( $_SERVER['REQUEST_URI'] )?>" method="post">
    <div id="new-form-header">
        <label for="thepassenger_form_new_name">Form Name</label>
        <input type="text" name="thepassenger_form_new_name" placeholder="Form name">
        <input type="hidden" value="" id="new-form-html" name="new-form-html">
        <input type="hidden" value="" id="new-form-data-type" name="new-form-data-type">
        <button type="submit" id="create-form-button">Create Form</button>
    </div>
    <p> Here it's how the form will look like.</p>
</form>