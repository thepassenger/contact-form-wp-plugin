var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: {
    'newForm': './js/app.js',
    'editForm': './js/edit.js',
    'deleteForm': './js/delete.js',
    'messagesPage_v2': './js/messages_v2.js'
  },
  output: {
    path: path.resolve(__dirname, './js/'),
    publicPath: '/js/',
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  }
}