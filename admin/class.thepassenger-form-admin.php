<?php

//require_once(dirname(__FILE__) . '/../class.thepassenger-form.php');
require_once(dirname(__FILE__) . '/../thepassenger-helpers.php');

class ThePassengerFormAdmin
{

    // protected static $initiated;
    public function __construct()
    {
        // $this->initiated = false;
    }
    // public static function init(){
    //     if ( ! self::$initiated ) {
    // 		self::init_hooks();
    // 	}
    // }

    public static function init()
    {
        // self::$initiated = true;
        add_action('admin_menu', array('ThePassengerFormAdmin', 'load_admin_menu'));

        add_action('admin_enqueue_scripts', array('ThePassengerFormAdmin', 'tp_admin_load_js'));
        add_action('wp_ajax_thepassenger_create_new_form', array('ThePassengerFormAdmin', 'thepassenger_create_new_form'));
        add_action('wp_ajax_thepassenger_edit_form', array('ThePassengerFormAdmin', 'thepassenger_edit_form'));
        add_action('wp_ajax_thepassenger_delete_form', array('ThePassengerFormAdmin', 'thepassenger_delete_form'));


        // static::tp_admin_load_js();
    }

    public static function load_admin_menu()
    {
        add_menu_page('Contact Form Options', 'Contact Forms', 'manage_options', 'thepassenger-contact-form-admin', array('ThePassengerFormAdmin', 'show_plugin_page'));

        add_submenu_page('thepassenger-contact-form-admin', 'Messages', 'Messages', 'manage_options', 'thepassenger-contact-form-admin-messages', array('ThePassengerFormAdmin', 'show_messages'));

        add_submenu_page('thepassenger-contact-form-admin', 'Create new form', 'Create new form', 'manage_options', 'thepassenger-contact-form-admin-add-new', array('ThePassengerFormAdmin', 'create_new_form'));

        add_submenu_page(null, 'Edit form', 'Edit form', 'manage_options', 'thepassenger-cf-admin-edit', array('ThePassengerFormAdmin', 'edit'));
    }

    public static function show_plugin_page()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        if (isset($_GET['form']) && isset($_GET['action']) && $_GET['action'] === 'edit') {
            return require_once(dirname(__FILE__) . '/../views/thepassenger-cf-admin-edit.php');
        }

        require_once(dirname(__FILE__) . '/../views/thepassenger-cf-admin-contact-forms_v2.php');


//        require_once(dirname(__FILE__) . '/../views/thepassenger-cf-admin-contact-forms.php');

        static::thepassenger_delete_form();
    }

    public static function thepassenger_delete_form()
    {
        global $wpdb;

        if (isset($_POST["formId"])) {
            $id = sanitize_text_field($_POST["formId"]);
            if (!is_numeric($id)) wp_die();

            $wpdb->delete($wpdb->prefix . 'thepassenger_forms', array('id' => $id));

            if (isset($_POST["deleteMessages"]) && $_POST["deleteMessages"] === 'true') {

                $shortcode = sanitize_text_field($_POST["shortcode"]);

                if (preg_match('/\s/', $shortcode)) wp_die();

                $sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . $shortcode . "_messages;";

                $wpdb->query($sql);
            }

            wp_die();

        }
    }

    public static function show_messages()
    {
        require_once(dirname(__FILE__) . '/../views/thepassenger-cf-admin-messages.php');
//        static::thepassenger_mark_messages_as_viewed();
    }

    public static function create_new_form()
    {
        require_once(dirname(__FILE__) . '/../views/thepassenger-cf-admin-create-new_v2.php');
        static::thepassenger_create_new_form();

    }

    public static function thepassenger_create_new_form()
    {
        global $wpdb; // this is how you get access to the database

        if (isset($_POST['form'])) {
            $data = json_decode(stripslashes_deep($_POST['form']), true);
            $name = sanitize_text_field($data['name']);
            $isNameNotUnique = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->prefix" . "thepassenger_forms where name LIKE '%$name%'");
            $name =  $name ? $name . ($isNameNotUnique ? "_" . ($isNameNotUnique + 1) : "") : "thepassenger_form_" . ($wpdb->get_var("SELECT COUNT(*) FROM $wpdb->prefix" . "thepassenger_forms where name LIKE '%thepassenger_form%'") + 1);
            //     $html = '<form method="post" action="#"><input type="hidden" name="tp_form_submitted">' . stripslashes_deep($_POST['new-form-html']) . '</form>';
            $shortcode = preg_replace('/[^a-zA-Z0-9]/', '_', $name);
            $html = ThePassengerForm::html($data);

            try {
                $wpdb->insert($wpdb->prefix . 'thepassenger_forms', array('name' => $name, 'shortcode' => $shortcode, 'structure' => json_encode($data), 'html' => $html));

                ThePassengerForm::createMessagesTable($data, $shortcode);

                // echo "Form has been created";
//                wp_die($shortcode);

            } catch (Exception $e) {
                echo $e;
            };

            wp_die($shortcode);
        }

    }

    public static function edit()
    {
        require_once(dirname(__FILE__) . '/../views/thepassenger-cf-admin-edit.php');
        static::thepassenger_edit_form();

    }

    /**
     * Modify the form table by performing 3 actions.
     * 1 -    Change the html and structure fields of the form in the 'thepassenger_forms' table
     * 2 -    Drop the old form messages database table. That means all old messages of previous form version are deleted.
     * 3 -    Create the new messages table according to the new form.
     */
    public static function thepassenger_edit_form()
    {
        global $wpdb;

        if (isset($_POST['form'])) {

            $data = json_decode(stripslashes_deep($_POST['form']), true);
            $name = sanitize_text_field($data['name']);
            $shortcode = preg_replace('/[^a-zA-Z0-9]/', '_', $name);
            $html = ThePassengerForm::html($data);

            try {

                $wpdb->update($wpdb->prefix . 'thepassenger_forms', array('structure' => json_encode($data), 'html' => $html), array('name' => $name));

                $sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . $shortcode . "_messages;";

                $wpdb->query($sql);

                ThePassengerForm::createMessagesTable($data, $shortcode);


            } catch (Exception $e) {
                echo $e;
            };
        }
    }

    public static function duplicate($form, $messages = false)
    {
//        wp_die(var_dump($form));
        global $wpdb;
//        $isNameUnique = $wpdb->get_results("SELECT * FROM $wpdb->prefix" . "thepassenger_forms where name LIKE '%" . $form->name . "_copy%'");
        $isNameUnique = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->prefix" . "thepassenger_forms where name LIKE '%" . $form->name . "_copy%'");
//        die(var_dump($isNameUnique));

//        $name =  $isNameUnique ? $form->name . '_copy' : (is_numeric(substr($form->name, -1)) ? '_copy_' . substr($form->name, -1) + 1 : '_copy_1');
        $name = $isNameUnique ? $form->name . "_copy_" . ($isNameUnique + 1) : $form->name . '_copy';
        $shortcode = $isNameUnique ? $form->shortcode . "_copy_" . ($isNameUnique + 1) : $form->shortcode . '_copy';
//        $shortcode =  $isNameUnique ? $form->shortcode . '_copy' : $form->shortcode . (is_numeric(substr($form->shortcode, -1)) ? '_copy_' . substr($form->shortcode, -1) + 1 : '_copy_1');

//        $name = $form->name . '_copy';
//        $shortcode = $form->shortcode . '_copy';

        try {
            $wpdb->insert($wpdb->prefix . 'thepassenger_forms', array(
                'name' => $name,
                'shortcode' => $shortcode,
                'structure' => $form->structure,
                'html' => $form->html));

//             echo "Form has been created";
//                wp_die($shortcode);

            if ($messages) {
                $data = json_decode($form->structure, true);
                ThePassengerForm::createMessagesTable($data, $shortcode);
            }
        } catch (Exception $e) {
            echo $e;
        };
    }
    public static function thepassenger_mark_messages_as_viewed()
    {
//        wp_die(var_dump($_SERVER["REQUEST_URI"]));

        global $wpdb;

        if (isset($_POST['tp_mark_submitted'])) {
//            die(var_dump($_POST));
            foreach ($_POST as $key => $value) {
                if ($value == 'checked') {
                    $shortcode = sanitize_text_field($_POST['tp_form_shortcode']);
                    if (preg_match('/\s/', $shortcode)) wp_die();

                    $wpdb->update($wpdb->prefix . $shortcode . '_messages', ['viewed' => 1], ['id' => $key]);
                }
            };
        }

    }

    public static function tp_admin_load_js($hook)
    {
//        wp_die($hook);
        if ($hook == 'contact-forms_page_thepassenger-contact-form-admin-add-new') {
            wp_register_script('thepassenger-cf-admin-new-form', plugins_url('../js/newForm.js', __FILE__), array('jquery', 'jquery-ui-sortable'));
//            wp_register_script('thepassenger-cf-admin-new-form-sortable', plugins_url('../js/newForm.js', __FILE__), array('jquery-ui-sortable'));
            wp_enqueue_script('thepassenger-cf-admin-new-form');
//            wp_enqueue_script('thepassenger-cf-admin-new-form-sortable');
            wp_localize_script('thepassenger-cf-admin-new-form', 'thepassenger_action', array('action' => 'thepassenger_create_new_form'));

        } else if ($hook == 'admin_page_thepassenger-cf-admin-edit') {
            wp_register_script('thepassenger-cf-admin-edit-form', plugins_url('../js/editForm.js', __FILE__), array('jquery', 'jquery-ui-sortable'));
//            wp_register_script('thepassenger-cf-admin-edit-form-sortable', plugins_url('../js/editForm.js', __FILE__), array('jquery-ui-sortable'));
            wp_enqueue_script('thepassenger-cf-admin-edit-form');
//            wp_enqueue_script('thepassenger-cf-admin-edit-form-sortable');
            wp_localize_script('thepassenger-cf-admin-edit-form', 'thepassenger_action', array('action' => 'thepassenger_edit_form'));
//        } else if ($hook == 'admin_page_thepassenger-cf-admin-edit') {
//            wp_register_script('thepassenger-cf-admin-edit-form', plugins_url('../js/editForm.js', __FILE__), array('jquery'));
//            wp_enqueue_script('thepassenger-cf-admin-edit-form');
//            wp_localize_script('thepassenger-cf-admin-edit-form', 'thepassenger_action', array('action' => 'thepassenger_edit_form'));
        } else if ($hook == 'toplevel_page_thepassenger-contact-form-admin') {
            wp_register_script('thepassenger-cf-admin-delete-form', plugins_url('../js/deleteForm.js', __FILE__), array('jquery'));
            wp_enqueue_script('thepassenger-cf-admin-delete-form');
            wp_localize_script('thepassenger-cf-admin-delete-form', 'thepassenger_action', array('action' => 'thepassenger_delete_form'));
        } else if ($hook == 'contact-forms_page_thepassenger-contact-form-admin-messages') {
            wp_register_script('thepassenger-cf-admin-messages', plugins_url('../js/messagesPage_v2.js', __FILE__), array('jquery'));
            wp_enqueue_script('thepassenger-cf-admin-messages');
//            wp_localize_script('thepassenger-cf-admin-messages', 'thepassenger_action', array('action' => 'thepassenger_delete_form'));
        }
        wp_register_style('thepassenger-cf-css', plugins_url('../css/main.css', __FILE__));
        wp_enqueue_style('thepassenger-cf-css');
    }
}