import Field from './class/Field.js';
import Form from './class/Form.js';
import generate from './generators/LoadGenerators.js';

jQuery(document).ready(function($) {

    $('#success-message').hide();
    // class Test {
    //     constructor(){
    //         this.a = 'asd';
    //         this.b = 'asd';
    //     }
    // }
    // var test = new Test();
    // var test2 = new Test();
    // console.log(test === test2);
    // console.log(test);
    var field = null;

    var formStructure = $('#form-structure').val();
    var form = new Form(formStructure ? JSON.parse(formStructure) : {
        name: '',
        fields: []
    });

    form.show();
    // Get the modal
    var modal = $('#myModal');
    // dataType = [];
    // When the user clicks on the button, open the modal 

    $('.generator').click(function(event) {
        let fieldType = $(event.target).attr('data-type');
        field = new Field(generate(fieldType).data());
        field.show();
    });

    // $(document).on('click', '.remove-field', function(event){
    //     console.log(form);

    // });

    // When the user clicks anywhere outside of the modal, close it
    $(window).on('click', function(event) {
        if (event.target == modal[0]) {
            modal.hide();
            $('.modal-content').empty();
        }
    });


    $(document).on('click', '#add-field-button', createField);


    // $('#text-generator').click(function(){
    //     addTextField(); // Generates the field json.
    // });

    // function addTextField() {
    //     $('.modal-content').append(field.show());
    // }

    function createField() {
        field.save();
        form.addField(field);
        form.show();
        $('.modal-content').empty();
        modal.hide();
    }

    $('#create-form-button').on('click', function(event) {
        form.name = $('input[name="thepassenger_form_new_name"]').val();
        form.submit(thepassenger_action.action);
    });

    $( ".fields" ).sortable({
        revert: true,
        update: function(event, ui) {
            let currentOrder = $(this).sortable('toArray');
            form.moveFields(currentOrder);
        },
    });
});

// {
//     type: 'text',
//     tag: 'input'
//     attrs: [
//         id: 'myId',
//         class: 'myClass',
//         placeholder: 'type ur name here',
//         required: true
//     ],
//     extra: [
//         label: 'My label',

//     ]
// }
