import capitalize from '../helpers.js';

class Field {
    constructor(data){
        this.type = data['type'];
        this.tag = data['tag'];
        this.attrs = data['attrs'];
        this.extras = data['extras'];
    }

    data(){

        return {
            type: this.type,
            tag: this.tag,
            attrs: this.attrs,
            extras: this.extras 
        }
    }

    showParams(){
        let out = '';
        Object.keys(this.attrs).forEach(field => {
            if (field === 'required') {
                let checked = this.attrs[field] ? 'checked' : '';
                out += `
                    <div class="field-group">
                        <label>${capitalize(field)}?</label>
                        <input type="checkbox" name="field_required" ${checked}>
                    </div>
                `;
            } else {
                 out += `
                    <div class="field-group">
                        <label>${capitalize(field)}:</label>
                        <input type="text" name="field_${field}" value="${this.attrs[field]}">
                    </div>
                    `;
            }
        })

        Object.keys(this.extras).forEach(field => {
            if (field === 'label') {
                let checkedYes = this.extras[field] ? 'checked' : '';
                let checkedNo = !this.extras[field] ? 'checked' : '';
                out += `
                    <div class="field-group">
                        <label>Do you want a label?</label>
                        <input type="radio" name="ask_if_label" value="no" ${checkedNo}>No
                        <input type="radio" name="ask_if_label" value="yes" ${checkedYes}>Yes
                        <label>Text:</label>
                        <input type="text" name="extra_label" placeholder="Label Text" value="${this.extras[field]}">
                    </div>
                `;
            } else {
                out += `
                    <div class="field-group">
                        <label>Text</label>
                        <input type="text" name="extra_text" value="${this.extras[field]}">
                    </div>
                `;
            }
        });

        return out;
    }

    showEdit() {
        let out = this.showParams();
        out += '<button type="button" id="edit-field-button">Edit</button></div>';
        jQuery('.modal-content').append(out);
        jQuery('#myModal').show();
        return out;
    }

    show() {
        let out = this.showParams();
        out += '<button type="button" id="add-field-button">Create</button></div>';
        jQuery('.modal-content').append(out);
        jQuery('#myModal').show();
        return out;
    }

    /**
     * Field's actual HTML
     * @return {string} 
     */
    html(){
        let attrs = '';
        let extras_before = ''; 
        let extras_after = '';       

        Object.keys(this.attrs).forEach(attr => {
            attrs += `${attr}="${this.attrs[attr]}" `;
        });

        Object.keys(this.extras).forEach(extra => {
            if (extra === 'label') {
                extras_before += `
                <label ${this.attrs['id'] ? 'for="'+this.attrs['id']+'"' : ''}>
                    ${this.extras[extra]}
                </label>`;
            }
            else {
                extras_after += `${extra}="${this.extras[extra]} `;
            }
        });

        return  `
            <div class="field">
                ${extras_before}
                <${this.tag} type="${this.type}" ${attrs}>
                ${extras_after}
            </div>`;
    }
    /**
     * Create attributes of the field
     * @return void
     */
    createAttributes(){
        let newAttrs = {};
        let newExtras = {};
        
        Object.keys(this.attrs).forEach(field => {
            
            if (field === 'required') {
                newAttrs['required'] = jQuery('input[name="field_required"]')[0].checked;
            }
            else if (jQuery(`input[name="field_${field}"]`).val()) {
                newAttrs[field] = jQuery(`input[name="field_${field}"]`).val();
            }
        });

        Object.keys(this.extras).forEach(field => {
            if (field === 'label' && jQuery(`input[name="ask_if_label"]:checked`).val() === 'yes') {  
                newExtras['label'] = jQuery(`input[name="extra_${field}"`).val();
            } else if (jQuery(`input[name="extra_${field}"]`).val()) {
                newExtras[field] = jQuery(`input[name="extra_${field}"]`).val()
            }
        });

        this.attrs = newAttrs;
        this.extras = newExtras;
    }
    /**
     * Save this field
     * @return {object} Object describing this field structure.
     */
    save() {
        this.createAttributes();

        return {
            tag: this.tag,
            type: this.type,
            attrs: this.attrs,
            extras: this.extras
        };
    }

}

export default Field;
// clicco sul field -> si apre modal con dati gia' inseriti.
// come?
// ogni field controlla value del json in classe field es i prende i value.

// {
//     type: 'text',
//     tag: 'input'
//     attrs: {
//         id: 'myId',
//         class: 'myClass',
//         placeholder: 'type ur name here',
//         required: true
//     },
//     extra: {
//         label: 'My label',
        
//     }
// }
// textField -> modal -> Field('text').show() -> create -> Form.addField({})
// 
// create form -> Form.post()

// class Form {
//     constructor() {
//         this.fields = [];
//     }

//     addField(data) {
//         this.fields.push(data)
//     }

//     post() {
//         $.post(url, this.fields)
//     }
// }
