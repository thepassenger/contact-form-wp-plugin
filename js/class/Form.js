import Field from './Field.js';
import generate from '../generators/LoadGenerators.js';

/**
 * Class that rapresents the form being created/edited
 * @constructor
 * @param {Object} data - The object describing the form with form name, fields with their attributes.
 */
class Form {

    /**
     * @param {object} data
     * @property {string} name - The form name.
     * @property {array} fields - Array of objects describing each field tag, type and attributes.
     */
    constructor(data) {
        this.name = data['name'];
        this.fields = data['fields'];
        data.fields.map(field => {
            return new Field(generate(field.type).update(field).data())
        });
        // new Field(generate(field.type).update(field).data());
    }

    /**
     * Create an object from this class relevant properties.
     * @return {Object}
     */
    data() {
        return {
            name: this.name,
            fields: this.fields
        }
    }

    /**
     * Add a field to the form
     * @param {Field} field - A field object.
     * @return void
     */
    addField(field) {
        let fieldWithPk = this.addPk(field);
        this.fields.push(fieldWithPk);
    }

    /**
     * Show the form in the preview area.
     * @return {string} The form Html
     */
    show() {
        // let out = '';
        jQuery('#new-form .fields').empty();

        this.fields.forEach((field, index) => {
            let out = `<div class="field" id="${index}">
                ${field.tag.toUpperCase()} ${field.type.toUpperCase()} `
            Object.keys(field.attrs).forEach(attr => {
                if (field.attrs[attr]) out += `${attr}: ${field.attrs[attr]} `;
            });
            Object.keys(field.extras).forEach(extra => {
                if (field.extras[extra]) out += `${extra}: ${field.extras[extra]} `;
            });
            out += `</div>`;
            let fieldNode = jQuery(out).appendTo('#new-form .fields');
            this.addModifiers(fieldNode, field);
        })
        // return out;
    }

    /**
     * Add field modifiers to the form html output. Can click to remove a field or edit
     * @param {jQuery} node The node containing the field.
     * @param {Field} field
     */
    addModifiers(node, field) {
        let out = `
            <div class="modifiers">
                <button type="button" class="remove-field" data-pk="${field.pk}">Remove</button>
                <button type="button" class="edit-field" data-pk="${field.pk}">Edit</button>
            </div>`;

        let button = jQuery(node).append(out);
        var vm = this;
        jQuery(button).off();
        jQuery(`button.remove-field[data-pk="${field.pk}"]`).on('click', function(event) {
            vm.remove(field);
        });
        jQuery(`button.edit-field[data-pk="${field.pk}"]`).on('click', function(event) {
            vm.edit(field);
        });
    }

    /**
     * Find a field inside the array.
     * @param {array} array
     * @param {Field}
     */
    findInArray(array, field) {
        return array.findIndex(function(element, index, array) {
            return field.pk === element.pk;
        });
    }

    /**
     * Remove a field from the form.
     * @param {Field}
     */
    remove(field) {
        this.fields.splice(this.findInArray(this.fields, field), 1);
        this.show();
    }

    /**
     * Show edit field form.
     * @param {Field}
     */
    edit(field) {
        let modifiedField = new Field(generate(field.type).update(field).data());
        modifiedField.showEdit();

        var vm = this;
        jQuery('#edit-field-button').off();
        jQuery('#edit-field-button').on('click', function() {
            // vm.findInArray(this.attrs, field)
            vm.update(field, modifiedField.save());
        });
    }

    /**
     * Update a field.
     * @param {Field}
     */
    update(field, modifiedField) {
        let index = this.findInArray(this.fields, field);
        this.fields[index] = this.addPk(modifiedField, field.pk);
        this.show();
        jQuery('.modal-content').empty();
        jQuery('#myModal').hide();
    }

    /**
     * Adds a primary key to a field just to identify it better inside the class.
     * @param {Field}
     * @param {int} The pk in case you want to provide it yourself.
     * @return {Field}
     */
    addPk(field, pk = null) {
        field.pk = pk ? pk : this.fields.length;
        return field;
    }

    /**
     * Submit the form with a post request.
     * @param {string} action
     */
    submit(action) {
        var vm = this;
        jQuery.post(ajaxurl, {
            'action': action,
            'form': JSON.stringify(this.data())
        }, function(data) {
            jQuery('#success-message span').text(data);
            jQuery('#success-message').show();
        });
        // jQuery.post(url, {'form': 'stocazzo'});
        // jQuery.ajax({
        //     type: 'POST',
        //     url: ajaxurl,
        //     data: {
        //         'action': 'thepassenger_create_new_form',
        //         'form': JSON.stringify(this.data())
        //     },
        //     success: function(data) {
        //         jQuery('#success-message').show();
        //     }
        // });
    }

    moveFields(newOrder){
        let newFields = [];
        newOrder.forEach((number, index) => {
            newFields[index] = this.fields[parseInt(number)];
        })
        this.fields = newFields;
    }
}
;

//Props:
//  fields: []
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 

export default Form;