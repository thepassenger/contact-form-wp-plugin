class Button {
    constructor() {
        this.type = 'submit'
        this.tag = 'button'
        this.attrs = {
            id: '',
            class: ''
        }
        this.extras = {
            text: ''
        }
    }

    update(data) {
        Object.keys(data.attrs).forEach(attr => {
            this.attrs[attr] = data.attrs[attr];
        });

        Object.keys(data.extras).forEach(extra => {
            this.extras[extra] = data.extras[extra];
        });

        return this;
    }

    data() {
        return {
            type: this.type,
            tag: this.tag,
            attrs: this.attrs,
            extras: this.extras
        }
    }
}

export default Button;
