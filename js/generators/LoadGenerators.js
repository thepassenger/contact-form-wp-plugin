import Text from './Text.js';
import Email from './Email.js';
import Password from './Password.js';
import Radio from './Radio.js';
import Checkbox from './Checkbox.js';
import Button from './Button.js';
import Color from './Color.js';
import Date from './Date.js';
import DatetimeLocal from './DatetimeLocal.js';
import Month from './Month.js';
import Url from './Url.js';
import TextArea from './TextArea.js';
import Number from './Number.js';
import Hidden from './Hidden.js';


var generators = {
    'text': function() {
        return new Text()
    },
    'email': function() {
        return new Email()
    },
    'password': function() {
        return new Password()
    },
    'radio': function() {
        return new Radio()
    },
    'checkbox': function() {
        return new Checkbox()
    },
    'button': function() {
        return new Button()
    },
    'color': function() {
        return new Color()
    },
    'date': function() {
        return new Date()
    },
    'datetimelocal': function() {
        return new DatetimeLocal()
    },
    'month': function() {
        return new Month()
    },
    'url': function() {
        return new Url()
    },
    'textarea': function() {
        return new TextArea()
    },
    'number': function() {
        return new Number()
    },
    'hidden': function() {
        return new Hidden()
    }
}

function generate(name) {
    switch  (name) {
        case "":
            return generators['textarea']();
        case "submit":
            return generators['button']();
        default:
            return generators[name]();
    }
}


export default generate;
// export {
//     newText,
//     newEmail,
//     newPassword,
//     newRadio,
//     newCheckbox, 
//     newButton, 
//     newColor, 
//     newDate, 
//     newDatetimeLocal, 
//     newMonth, 
//     newUrl, 
//     newTextArea, 
//     newNumber, 
//     newHidden
// };
