jQuery(document).ready(function($) {

    var modal = $('#myModal')

    $(".delete-form-button").click(function() {
        let formId = $(this).parent().find("input[name='formId']").val();
        let shortCode = $(this).parent().find("input[name='formShortCode']").val();
        let deleteMessages = $('#delete-messages')[0].checked;
        let row = $('tr').has(`button[data-formId=${formId}]`);
        deleteForm(formId, row, deleteMessages, shortCode);
    });

    $(".open-modal-button").click(function() {
        // <div class="delete-messages"><input type="checkbox" id="delete-messages">Delete also the messages table</div>"

        let out = `
            <div class="delete-form-text">
                <h1>Are you sure you want to delete the ` + $(this).attr('data-formName') + `form?</h1>
                <div class="delete-messages">
                    <input type="checkbox" id="delete-messages">
                    <label for="delete-messages">
                        Delete also the messages table
                    </label>
                </div>
            </div>
        `
        $('.modal-content').prepend(out);

        $('.modal-content input[name="formId"]')[0].value = $(this).attr('data-formId');
        $('.modal-content input[name="formShortCode"]')[0].value = $(this).attr('data-formShortCode');
        modal.show();

    })

    /**
     * Delete the form with an ajax post request. Uses Wordpress native ajax handling ajax functionality.
     * @param {string} id
     * @param {jQuery} row
     * @param {boolean} deleteMessages
     * @param {string} shortCode
     */
    function deleteForm(id, row, deleteMessages, shortCode) {
        $.post(ajaxurl, {
            'action': thepassenger_action.action,
            'formId': id,
            'deleteMessages': deleteMessages,
            'shortcode': shortCode
        }, function(data) {
            row.remove();
            closeModal();
        });
    }

    $('.close-modal-button').on('click', closeModal);

    $(window).on('click', function(event) {
        if (event.target == modal[0]) {
            closeModal();
        }
    });

    function closeModal() {
        modal.hide();
        $('.modal-content input[type="hidden"]')[0].value = 0;
        $('.modal-content .delete-form-text').empty();
    }

});