import Field from './class/Field.js';
import Form from './class/Form.js';
import generate from './generators/LoadGenerators.js';

jQuery(document).ready(function($) {

    $('#success-message').hide();

    var field = null;

    var formStructure = JSON.parse($('#form-structure').val());
    // if (! formStructure.name) formStructure.name = $('input[name="thepassenger_form_name"]').val();
    var form = new Form(formStructure);

    form.show();
    var modal = $('#myModal');

    // When the user clicks on the button, open the modal

    $('.generator').click(function(event) {
        let fieldType = $(event.target).attr('data-type');
        field = new Field(generate(fieldType).data());
        field.show();
    });

    // When the user clicks anywhere outside of the modal, close it
    $(window).on('click', function(event) {
        if (event.target == modal[0]) {
            modal.hide();
            $('.modal-content').empty();
        }
    });



    $(document).on('click','#add-field-button', createField);


    function createField() {
        field.save();
        form.addField(field);
        form.show();
        $('.modal-content').empty();
        modal.hide();
    }


    $('#edit-form-button').on('click', function(event){
        form.name = $('input[name="thepassenger_form_name"]').val();
        form.submit(thepassenger_action.action);
    });

    $( ".fields" ).sortable({
        revert: true,
        update: function(event, ui) {
            let currentOrder = $(this).sortable('toArray');
            form.moveFields(currentOrder);
            },
    });

    // $( ".field" ).draggable({
    //     connectToSortable: ".fields",
    // //     drag: function( event, ui ){},
    // //     snap: '.fields'
    // }).on( "drag", function( event, ui ) {console.log('lul');} );
});
