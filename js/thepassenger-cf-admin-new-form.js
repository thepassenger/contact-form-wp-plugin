jQuery(document).ready(function($) {
    // Get the modal
    var modal = $('#myModal');

    dataType = [];
    // When the user clicks on the button, open the modal 
    $('.generator').click(function() {
        modal.show()
    });


    // When the user clicks anywhere outside of the modal, close it
    $(window).on('click', function(event) {
        if (event.target == modal[0]) {
            modal.hide();
            $('.modal-content').empty();
        }
    });

    $('#text-generator').click(function(){
        addTextField();
        $('.show_max_chars').hide();
    });

    $('#email-generator').click(function(){
        addEmailField();
    });

    $('#password-generator').click(function(){
        addPasswordField();
    });

    $('#radio-generator').click(function(){
        addRadioField();
    });

    $('#checkbox-generator').click(function(){
        addCheckboxField();
    });

    $('#button-generator').click(function(){
        askTypeOfButton();
    });

    $('#color-generator').click(function(){
        addColorField();
    });

    $('#date-generator').click(function(){
        addDateField();
    });

    $('#datetime-local-generator').click(function(){
        addDatetimeLocalField();
    });

    $('#month-generator').click(function(){
        addMonthField();
    });

    $('#number-generator').click(function(){
        addNumberField();
    });

    $('#url-generator').click(function(){
        addUrlField();
    });

    $('#textArea-generator').click(function(){
        addTextAreaField();
    });

    $('#hidden-generator').click(function(){
        addHiddenField();
    });

    $(document).on('change','#text-options', showMaxChars);
    $(document).on('click','#add-field-button', createField);
    $(document).on('click','#add-textarea-button', createTextArea);    
    $(document).on('click','#create-button-field', addButtonField);

    // $('#create-form-button').click(function(){
    //     $('#new-form-html').value= $('#new-form-header').siblings('div')
    // })
    
    function createField(event) {
        let newInput = $(event.target).parent().parent();
        let type = $(event.target).attr('data-type');
        let attributes = createAttributesFields(newInput, type);        
        let fieldId = newInput.find('input[name="field_id"]')[0] ? newInput.find('input[name="field_id"]')[0].value : '';
        let extraFields = createExtraFields(newInput, fieldId);
        let name = newInput.find('input[name="field_name"]')[0] ? newInput.find('input[name="field_name"]')[0].value : null;
        
        let fieldHtml = "<div class='field'> \
            "+extraFields[0]+"<input type='"+type+"' "+attributes+">"+extraFields[1]+"</div>";
        $('#new-form-html')[0].value += fieldHtml;
        // if ((type==='radio' || type==="checkbox") && $('input[type="' + type +"][name") 
        let required = fieldHtml.search(/ required/) === -1 ? false : true;
        dataType.push([type, name, required]);
        $('#new-form-data-type')[0].value = JSON.stringify(dataType);
        $('#new-form').append(fieldHtml.replace(/ required/, ''));
        $('.modal-content').empty();
        modal.hide();
    }

    function createExtraFields(newInput, fieldId) {
        let extras = newInput.parent().find('input[name*="extra_"]');        
        extra_before = '';
        extra_after = '';
        extras.each(function(index){
            if (extras[index].value) {
                let name_of_field = extras[index]['name'].split('extra_')[1];
                if (name_of_field === 'label' && 
                    newInput.find('input[name*="ask_if_label"]:checked')[0].value === 'yes') {
                        extra_before += '<label for="'+fieldId+'">'+extras[index].value+'</label>';
                }
                if (name_of_field === 'text') {
                    extra_after += extras[index].value+'</br>';
                }
            }
        });
        
        return [extra_before, extra_after];
    }

    function createAttributesFields(newInput, type){
        let fields = newInput.find('input[name*="field_"]');
        let attributes = '';
        fields.each(function(index){
            if (fields[index].value) {
                let name_of_field = fields[index]['name'].split('field_')[1];
                if (name_of_field === 'required'){
                       if (fields[index].checked) attributes += 'required';
                } 
                else if (name_of_field === 'name' && type === 'checkbox') {
                    attributes += name_of_field + '="'+ fields[index].value+'[]" ';
                } else {
                    attributes += name_of_field + '="'+ fields[index].value+'" ';
                }
            }
        });

        return attributes;
    }


    function createTextArea(event) {
        let newInput = $(event.target).parent();
        let fields = newInput.find('input[name*="field_"]');
        let attributes = createAttributesFields(newInput);        
        let fieldId = newInput.find('input[name="field_id"]')[0] ? newInput.find('input[name="field_id"]')[0].value : '';
        let extraFields = createExtraFields(newInput, fieldId);
        let name = newInput.find('input[name="field_name"]')[0] ? newInput.find('input[name="field_name"]')[0].value : null;
        
        // let fieldHtml = "<div class='field'> \
        //     "+extraFields[0]+"<input type='"+type+"' "+attributes+">"+extraFields[1]+"</div>";
        // let name = '';
        // attributes = '';
        // fields.each(function(index){
        //     if (fields[index].value) {
        //         let name_of_field = fields[index]['name'].split('field_')[1];
        //         if (name_of_field === 'required'){
        //                if (fields[index].checked) attributes += 'required';
        //         } else {
        //             if (name_of_field === 'name') name = fields[index].value;
        //             attributes += name_of_field + '="'+ fields[index].value+'" ';
        //         }
        //     }
        // });
        let fieldHtml = '<div class="field">'+extraFields[0]+'<textarea '+attributes+'></textarea>'+extraFields[1]+'</div>';
        
        $('#new-form-html')[0].value += fieldHtml;
        let required = fieldHtml.search(/ required/) === -1 ? false : true;
        dataType.push(['textarea', name, required]); //check
        $('#new-form-data-type')[0].value = JSON.stringify(dataType);
        
        $('#new-form').append(fieldHtml.replace(/ required/, ''));
        // $('#new-form').append(fieldHtml);
        
        $('.modal-content').empty();
        modal.hide();
    }

    function addTextField(){
        $('.modal-content').append('<div class="field"><div class="field-group"><span>Select type:</span> \
                <select id="text-options"> \
                    <option  id="option-text" value="text">Text</option>\
                    <option  id="option-varchar" value="varchar">Var Char</option>\
                </select>\
                <label class="show_max_chars" for="maxChars">Max chars (255 default)</label>\
                <input class="show_max_chars" type="number" name="maxChars"></div>' 
                + basicAttrInputs + askSubmitButton('text') + '</div>');
    }

    function addEmailField(){
        $('.modal-content').append('<div class="field">' + basicAttrInputs + askSubmitButton('email') + '</div>');
    };

    function addPasswordField(){
        $('.modal-content').append('<div class="field">' + basicAttrInputs + askSubmitButton('password') + '</div>');
    }

    function addRadioField(){
        $('.modal-content').append('<div class="field">' + askValue + '<div class="field-group">\
        <label for="extra_text">Radio Text</label><input type="text" name="extra_text"></div>'
        + basicAttrInputs + askSubmitButton('radio') + '</div>')
    }

    function addCheckboxField(){
        $('.modal-content').append('<div class="field">' + askValue + '<div class="field-group">\
        <label for="extra_text">Checkbox Text</label><input type="text" name="extra_text"></div>'
        + basicAttrInputs + askSubmitButton('checkbox') + '</div>')

    }

    function askTypeOfButton(){
        $('.modal-content').append('<div class="field"><div class="field-group"> \
                <h4>Select a type of button</h4> \
                <input type="radio" name="button_type" value="reset"> Reset\
                <input type="radio" name="button_type" value="submit"> Submit\
                <input type="radio" name="button_type" value="button"> Button\
                <button type="button" id="create-button-field">Create</button></div></div>');
    }
    
    function addButtonField(event){
        let buttonTypeField = $(event.target).parent()
        let buttonType = buttonTypeField.find('input[name="button_type"]:checked')[0].value;
        $('.modal-content').append(askClass + askId + askSubmitButton(buttonType));
        // $('.modal-content').find('input[name="field_name"]')[0].value = 'tp_form_submitted';
        
    }
    
    function addColorField(){
        $('.modal-content').append('<div class="field">' + basicAttrInputs + askSubmitButton('color') + '</div>');
    }

    function addDateField(){
        $('.modal-content').append('<div class="field">' + basicAttrInputs + askSubmitButton('date') + '</div>');
    }

    function addDatetimeLocalField(){
        $('.modal-content').append('<div class="field">' + basicAttrInputs + askSubmitButton('datetime-local') + '</div>');
    }

    function addMonthField(){
        $('.modal-content').append('<div class="field">' + basicAttrInputs + askSubmitButton('month') + '</div>');
    }

    function addUrlField(){
        $('.modal-content').append('<div class="field">' + basicAttrInputs + askSubmitButton('url') + '</div>');
    }

    function addTextAreaField(){
        $('.modal-content').append('<div class="field"><div class="field-group"> \
                <label for="field_rows">Rows:</label> \
                <input type="number" name="field_rows"><label for="field_cols">Cols:</label> \
                <input type="number" name="field_cols">'
                + basicAttrInputs +
                '<button type="button" id="add-textarea-button">Add Field</button></div></div>');
    }

    function addNumberField(){
        $('.modal-content').append('<div class="field"><div class="field-group">\
        <label for="field_min">Min:</label><input type="number" name="field_min"><label for="field_max">Max:</label> \
            <input type="number" name="field_max"></div>' + basicAttrInputs + askSubmitButton('number') + '</div>');
    }

    function addHiddenField(){
        $('.modal-content').append('<div class="field">' + askName + askValue + askSubmitButton('hidden') + '</div>');
    }


    var askName = '<div class="field-group"><label for="field_name">Name:</label>\
            <input type="text" name="field_name"></div>';

    var askId = '<div class="field-group"><label for="field_id">Id:</label>\
            <input type="text" name="field_id"></div>';

    var askClass = '<div class="field-group"><label for="field_class">Class:</label>\
            <input type="text" name="field_class"></div>';

    var askPlaceholder = '<div class="field-group"><label for="field_placeholder">Placeholder</label>\
            <input type="text" name="field_placeholder"></div>';

    var askValue = '<div class="field-group"><label for="field_value">Value</label>\
        <input type="text" name="field_value"></div>';

    var askRequired = '<div class="field-group"><label for="field_required">Required?</label>\
        <input type="checkbox" name="field_required" value="required"></div>';

    var basicAttrInputs = askName + askClass + askId + askPlaceholder + askRequired +askForLabel();

    function askForLabel(){
        return '<div class="field-group"><label for="ask_if_label">Do you want a label?</label>\
            <input type="radio" name="ask_if_label" value="no" checked>No<br>\
            <input type="radio" name="ask_if_label" value="yes">Yes<br>\
            <label for="extra_label">Text</label>\
            <input type="text" name="extra_label" placeholder="Label text"></div>'
    }

    function askSubmitButton(type){
        return  '<div class="field-group"><button type="button" id="add-field-button" data-type="'+type+'">\
        Add Field</button></div>'
    }

    /**
     * Toggle the max characters field in case of text/varchar input field 
     * @param {Event} event 
     */
    function showMaxChars(event) {
        $('.show_max_chars').toggle();
    }

})
    // /**
    //  * Add a form to admin page where admin can select the attributes of his field
    //  * 
    //  * @param {string} name: The name of the field 
    //  * @param {string} type: the type of the field. Example <input type="text">
    //  * @param {string} customInput: extra attributes to add to the form. Example regex matching,
    //  *                              or labels for the field. This must be added in pure HTML
    //  * 
    //  */

    // function addField(name, type , customInput=''){
    //     $('.modal-content').append('<div class="field"> \
    //             <h4>'+name+' Input</h4> \
    //             '+customInput+'\
    //             <label for="field_id">Id:</label>\
    //             <input type="text" name="field_id">\
    //             <label for="field_name">Name:</label>\
    //             <input type="text" name="field_name">\
    //             <label for="field_class">Class</label>\
    //             <input type="text" name="field_class">\
    //             <label for="extra_label">Label</label>\
    //             <input type="text" name="extra_label">\
    //             <label for="field_placeholder">Placeholder</label>\
    //             <input type="text" name="field_placeholder">\
    //             <button type="button" id="add-field-button" data-type="'+type+'">Add Field</button>\
    //         </div>');
    // }

        // function createField(event){
    //     let type = $(event.target).attr('data-type');
    //     let newInput = $(event.target).parent().parent();
    //     let fields = newInput.find('input[name*="field_"]');
    //     let extras = newInput.find('input[name*="extra_"]');
    //     extra_before = '';
    //     attributes = '';
    //     field_name = '';
    //     extra_after = '';
    //     fields.each(function(index){
    //         if (fields[index].value) {
    //             let name_of_field = fields[index]['name'].split('field_')[1];
    //             attributes += fields[index]['name'].split('field_')[1] + '="'+ fields[index].value+'" '
    //             if (name_of_field === 'id') field_name = fields[index].value;
    //         }
    //     });

    //     extras.each(function(index){
    //         if (extras[index].value) {
    //             let name_of_field = extras[index]['name'].split('extra_')[1];
    //             if (name_of_field === 'label' && 
    //                 newInput.find('input[name*="ask_if_label"]:checked')[0].value === 'yes') {
    //                     extra_before += '<label for="'+field_name+'">'+extras[index].value+'</label>';
    //             }
    //             if (name_of_field === 'text') {
    //                 extra_after += extras[index].value+'</br>';
    //             }
    //         }
    //     })

        
    //     $('#new-form').append('<div class="field"> \
    //         '+extra_before+'<input type="'+type+'" '+attributes+'>'+extra_after+'</div>');

    //     $('.modal-content').empty();
    //     modal.hide();
    // }