jQuery(document).ready(function($) {
    $('.select-all-checkbox').click(function() {

        if (this.checked) {
            $(this).closest('tr').nextAll().find('input[type="checkbox"]').each((index, checkbox) => {
                $(checkbox).prop('checked', true);
            })
            return;
        }

        $(this).closest('tr').nextAll().find('input[type="checkbox"]').each((index, checkbox) => {
            $(checkbox).prop('checked', false);
        });


    })

    $('.tp-messages-form-submit').prop('disabled', true);

    $('input[type="checkbox"]').change(function(){
        let form = $(this).closest('form');
        let subButton = $(form).find('button');
        if ($.makeArray($(form).find('table input:not(:first)[type="checkbox"]')).some(isChecked)) {
            return subButton.prop("disabled", false);
        }

        subButton.prop("disabled", true);
        // if (form.has('input[type="checkbox"]').prop('checked', true)) {
        //     console.log('y');
        // }

    })

    var isChecked = function(element, index, array) {
        return element.checked;
    }
});
