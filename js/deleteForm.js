/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 19);
/******/ })
/************************************************************************/
/******/ ({

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


jQuery(document).ready(function ($) {

    var modal = $('#myModal');

    $(".delete-form-button").click(function () {
        var formId = $(this).parent().find("input[name='formId']").val();
        var shortCode = $(this).parent().find("input[name='formShortCode']").val();
        var deleteMessages = $('#delete-messages')[0].checked;
        var row = $('tr').has("button[data-formId=" + formId + "]");
        deleteForm(formId, row, deleteMessages, shortCode);
    });

    $(".open-modal-button").click(function () {
        // <div class="delete-messages"><input type="checkbox" id="delete-messages">Delete also the messages table</div>"

        var out = "\n            <div class=\"delete-form-text\">\n                <h1>Are you sure you want to delete the " + $(this).attr('data-formName') + "form?</h1>\n                <div class=\"delete-messages\">\n                    <input type=\"checkbox\" id=\"delete-messages\">\n                    <label for=\"delete-messages\">\n                        Delete also the messages table\n                    </label>\n                </div>\n            </div>\n        ";
        $('.modal-content').prepend(out);

        $('.modal-content input[name="formId"]')[0].value = $(this).attr('data-formId');
        $('.modal-content input[name="formShortCode"]')[0].value = $(this).attr('data-formShortCode');
        modal.show();
    });

    /**
     * Delete the form with an ajax post request. Uses Wordpress native ajax handling ajax functionality.
     * @param {string} id
     * @param {jQuery} row
     * @param {boolean} deleteMessages
     * @param {string} shortCode
     */
    function deleteForm(id, row, deleteMessages, shortCode) {
        $.post(ajaxurl, {
            'action': thepassenger_action.action,
            'formId': id,
            'deleteMessages': deleteMessages,
            'shortcode': shortCode
        }, function (data) {
            row.remove();
            closeModal();
        });
    }

    $('.close-modal-button').on('click', closeModal);

    $(window).on('click', function (event) {
        if (event.target == modal[0]) {
            closeModal();
        }
    });

    function closeModal() {
        modal.hide();
        $('.modal-content input[type="hidden"]')[0].value = 0;
        $('.modal-content .delete-form-text').empty();
    }
});

/***/ })

/******/ });