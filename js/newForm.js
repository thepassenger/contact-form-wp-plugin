/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 18);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _helpers = __webpack_require__(17);

var _helpers2 = _interopRequireDefault(_helpers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Field = function () {
    function Field(data) {
        _classCallCheck(this, Field);

        this.type = data['type'];
        this.tag = data['tag'];
        this.attrs = data['attrs'];
        this.extras = data['extras'];
    }

    _createClass(Field, [{
        key: 'data',
        value: function data() {

            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }, {
        key: 'showParams',
        value: function showParams() {
            var _this = this;

            var out = '';
            Object.keys(this.attrs).forEach(function (field) {
                if (field === 'required') {
                    var checked = _this.attrs[field] ? 'checked' : '';
                    out += '\n                    <div class="field-group">\n                        <label>' + (0, _helpers2.default)(field) + '?</label>\n                        <input type="checkbox" name="field_required" ' + checked + '>\n                    </div>\n                ';
                } else {
                    out += '\n                    <div class="field-group">\n                        <label>' + (0, _helpers2.default)(field) + ':</label>\n                        <input type="text" name="field_' + field + '" value="' + _this.attrs[field] + '">\n                    </div>\n                    ';
                }
            });

            Object.keys(this.extras).forEach(function (field) {
                if (field === 'label') {
                    var checkedYes = _this.extras[field] ? 'checked' : '';
                    var checkedNo = !_this.extras[field] ? 'checked' : '';
                    out += '\n                    <div class="field-group">\n                        <label>Do you want a label?</label>\n                        <input type="radio" name="ask_if_label" value="no" ' + checkedNo + '>No\n                        <input type="radio" name="ask_if_label" value="yes" ' + checkedYes + '>Yes\n                        <label>Text:</label>\n                        <input type="text" name="extra_label" placeholder="Label Text" value="' + _this.extras[field] + '">\n                    </div>\n                ';
                } else {
                    out += '\n                    <div class="field-group">\n                        <label>Text</label>\n                        <input type="text" name="extra_text" value="' + _this.extras[field] + '">\n                    </div>\n                ';
                }
            });

            return out;
        }
    }, {
        key: 'showEdit',
        value: function showEdit() {
            var out = this.showParams();
            out += '<button type="button" id="edit-field-button">Edit</button></div>';
            jQuery('.modal-content').append(out);
            jQuery('#myModal').show();
            return out;
        }
    }, {
        key: 'show',
        value: function show() {
            var out = this.showParams();
            out += '<button type="button" id="add-field-button">Create</button></div>';
            jQuery('.modal-content').append(out);
            jQuery('#myModal').show();
            return out;
        }

        /**
         * Field's actual HTML
         * @return {string} 
         */

    }, {
        key: 'html',
        value: function html() {
            var _this2 = this;

            var attrs = '';
            var extras_before = '';
            var extras_after = '';

            Object.keys(this.attrs).forEach(function (attr) {
                attrs += attr + '="' + _this2.attrs[attr] + '" ';
            });

            Object.keys(this.extras).forEach(function (extra) {
                if (extra === 'label') {
                    extras_before += '\n                <label ' + (_this2.attrs['id'] ? 'for="' + _this2.attrs['id'] + '"' : '') + '>\n                    ' + _this2.extras[extra] + '\n                </label>';
                } else {
                    extras_after += extra + '="' + _this2.extras[extra] + ' ';
                }
            });

            return '\n            <div class="field">\n                ' + extras_before + '\n                <' + this.tag + ' type="' + this.type + '" ' + attrs + '>\n                ' + extras_after + '\n            </div>';
        }
        /**
         * Create attributes of the field
         * @return void
         */

    }, {
        key: 'createAttributes',
        value: function createAttributes() {
            var newAttrs = {};
            var newExtras = {};

            Object.keys(this.attrs).forEach(function (field) {

                if (field === 'required') {
                    newAttrs['required'] = jQuery('input[name="field_required"]')[0].checked;
                } else if (jQuery('input[name="field_' + field + '"]').val()) {
                    newAttrs[field] = jQuery('input[name="field_' + field + '"]').val();
                }
            });

            Object.keys(this.extras).forEach(function (field) {
                if (field === 'label' && jQuery('input[name="ask_if_label"]:checked').val() === 'yes') {
                    newExtras['label'] = jQuery('input[name="extra_' + field + '"').val();
                } else if (jQuery('input[name="extra_' + field + '"]').val()) {
                    newExtras[field] = jQuery('input[name="extra_' + field + '"]').val();
                }
            });

            this.attrs = newAttrs;
            this.extras = newExtras;
        }
        /**
         * Save this field
         * @return {object} Object describing this field structure.
         */

    }, {
        key: 'save',
        value: function save() {
            this.createAttributes();

            return {
                tag: this.tag,
                type: this.type,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Field;
}();

exports.default = Field;
// clicco sul field -> si apre modal con dati gia' inseriti.
// come?
// ogni field controlla value del json in classe field es i prende i value.

// {
//     type: 'text',
//     tag: 'input'
//     attrs: {
//         id: 'myId',
//         class: 'myClass',
//         placeholder: 'type ur name here',
//         required: true
//     },
//     extra: {
//         label: 'My label',

//     }
// }
// textField -> modal -> Field('text').show() -> create -> Form.addField({})
// 
// create form -> Form.post()

// class Form {
//     constructor() {
//         this.fields = [];
//     }

//     addField(data) {
//         this.fields.push(data)
//     }

//     post() {
//         $.post(url, this.fields)
//     }
// }

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Text = __webpack_require__(14);

var _Text2 = _interopRequireDefault(_Text);

var _Email = __webpack_require__(8);

var _Email2 = _interopRequireDefault(_Email);

var _Password = __webpack_require__(12);

var _Password2 = _interopRequireDefault(_Password);

var _Radio = __webpack_require__(13);

var _Radio2 = _interopRequireDefault(_Radio);

var _Checkbox = __webpack_require__(4);

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _Button = __webpack_require__(3);

var _Button2 = _interopRequireDefault(_Button);

var _Color = __webpack_require__(5);

var _Color2 = _interopRequireDefault(_Color);

var _Date = __webpack_require__(6);

var _Date2 = _interopRequireDefault(_Date);

var _DatetimeLocal = __webpack_require__(7);

var _DatetimeLocal2 = _interopRequireDefault(_DatetimeLocal);

var _Month = __webpack_require__(10);

var _Month2 = _interopRequireDefault(_Month);

var _Url = __webpack_require__(16);

var _Url2 = _interopRequireDefault(_Url);

var _TextArea = __webpack_require__(15);

var _TextArea2 = _interopRequireDefault(_TextArea);

var _Number = __webpack_require__(11);

var _Number2 = _interopRequireDefault(_Number);

var _Hidden = __webpack_require__(9);

var _Hidden2 = _interopRequireDefault(_Hidden);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var generators = {
    'text': function text() {
        return new _Text2.default();
    },
    'email': function email() {
        return new _Email2.default();
    },
    'password': function password() {
        return new _Password2.default();
    },
    'radio': function radio() {
        return new _Radio2.default();
    },
    'checkbox': function checkbox() {
        return new _Checkbox2.default();
    },
    'button': function button() {
        return new _Button2.default();
    },
    'color': function color() {
        return new _Color2.default();
    },
    'date': function date() {
        return new _Date2.default();
    },
    'datetimelocal': function datetimelocal() {
        return new _DatetimeLocal2.default();
    },
    'month': function month() {
        return new _Month2.default();
    },
    'url': function url() {
        return new _Url2.default();
    },
    'textarea': function textarea() {
        return new _TextArea2.default();
    },
    'number': function number() {
        return new _Number2.default();
    },
    'hidden': function hidden() {
        return new _Hidden2.default();
    }
};

function generate(name) {
    switch (name) {
        case "":
            return generators['textarea']();
        case "submit":
            return generators['button']();
        default:
            return generators[name]();
    }
}

exports.default = generate;
// export {
//     newText,
//     newEmail,
//     newPassword,
//     newRadio,
//     newCheckbox, 
//     newButton, 
//     newColor, 
//     newDate, 
//     newDatetimeLocal, 
//     newMonth, 
//     newUrl, 
//     newTextArea, 
//     newNumber, 
//     newHidden
// };

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Field = __webpack_require__(0);

var _Field2 = _interopRequireDefault(_Field);

var _LoadGenerators = __webpack_require__(1);

var _LoadGenerators2 = _interopRequireDefault(_LoadGenerators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Class that rapresents the form being created/edited
 * @constructor
 * @param {Object} data - The object describing the form with form name, fields with their attributes.
 */
var Form = function () {

    /**
     * @param {object} data
     * @property {string} name - The form name.
     * @property {array} fields - Array of objects describing each field tag, type and attributes.
     */
    function Form(data) {
        _classCallCheck(this, Form);

        this.name = data['name'];
        this.fields = data['fields'];
        data.fields.map(function (field) {
            return new _Field2.default((0, _LoadGenerators2.default)(field.type).update(field).data());
        });
        // new Field(generate(field.type).update(field).data());
    }

    /**
     * Create an object from this class relevant properties.
     * @return {Object}
     */


    _createClass(Form, [{
        key: 'data',
        value: function data() {
            return {
                name: this.name,
                fields: this.fields
            };
        }

        /**
         * Add a field to the form
         * @param {Field} field - A field object.
         * @return void
         */

    }, {
        key: 'addField',
        value: function addField(field) {
            var fieldWithPk = this.addPk(field);
            this.fields.push(fieldWithPk);
        }

        /**
         * Show the form in the preview area.
         * @return {string} The form Html
         */

    }, {
        key: 'show',
        value: function show() {
            var _this = this;

            // let out = '';
            jQuery('#new-form .fields').empty();

            this.fields.forEach(function (field, index) {
                var out = '<div class="field" id="' + index + '">\n                ' + field.tag.toUpperCase() + ' ' + field.type.toUpperCase() + ' ';
                Object.keys(field.attrs).forEach(function (attr) {
                    if (field.attrs[attr]) out += attr + ': ' + field.attrs[attr] + ' ';
                });
                Object.keys(field.extras).forEach(function (extra) {
                    if (field.extras[extra]) out += extra + ': ' + field.extras[extra] + ' ';
                });
                out += '</div>';
                var fieldNode = jQuery(out).appendTo('#new-form .fields');
                _this.addModifiers(fieldNode, field);
            });
            // return out;
        }

        /**
         * Add field modifiers to the form html output. Can click to remove a field or edit
         * @param {jQuery} node The node containing the field.
         * @param {Field} field
         */

    }, {
        key: 'addModifiers',
        value: function addModifiers(node, field) {
            var out = '\n            <div class="modifiers">\n                <button type="button" class="remove-field" data-pk="' + field.pk + '">Remove</button>\n                <button type="button" class="edit-field" data-pk="' + field.pk + '">Edit</button>\n            </div>';

            var button = jQuery(node).append(out);
            var vm = this;
            jQuery(button).off();
            jQuery('button.remove-field[data-pk="' + field.pk + '"]').on('click', function (event) {
                vm.remove(field);
            });
            jQuery('button.edit-field[data-pk="' + field.pk + '"]').on('click', function (event) {
                vm.edit(field);
            });
        }

        /**
         * Find a field inside the array.
         * @param {array} array
         * @param {Field}
         */

    }, {
        key: 'findInArray',
        value: function findInArray(array, field) {
            return array.findIndex(function (element, index, array) {
                return field.pk === element.pk;
            });
        }

        /**
         * Remove a field from the form.
         * @param {Field}
         */

    }, {
        key: 'remove',
        value: function remove(field) {
            this.fields.splice(this.findInArray(this.fields, field), 1);
            this.show();
        }

        /**
         * Show edit field form.
         * @param {Field}
         */

    }, {
        key: 'edit',
        value: function edit(field) {
            var modifiedField = new _Field2.default((0, _LoadGenerators2.default)(field.type).update(field).data());
            modifiedField.showEdit();

            var vm = this;
            jQuery('#edit-field-button').off();
            jQuery('#edit-field-button').on('click', function () {
                // vm.findInArray(this.attrs, field)
                vm.update(field, modifiedField.save());
            });
        }

        /**
         * Update a field.
         * @param {Field}
         */

    }, {
        key: 'update',
        value: function update(field, modifiedField) {
            var index = this.findInArray(this.fields, field);
            this.fields[index] = this.addPk(modifiedField, field.pk);
            this.show();
            jQuery('.modal-content').empty();
            jQuery('#myModal').hide();
        }

        /**
         * Adds a primary key to a field just to identify it better inside the class.
         * @param {Field}
         * @param {int} The pk in case you want to provide it yourself.
         * @return {Field}
         */

    }, {
        key: 'addPk',
        value: function addPk(field) {
            var pk = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            field.pk = pk ? pk : this.fields.length;
            return field;
        }

        /**
         * Submit the form with a post request.
         * @param {string} action
         */

    }, {
        key: 'submit',
        value: function submit(action) {
            var vm = this;
            jQuery.post(ajaxurl, {
                'action': action,
                'form': JSON.stringify(this.data())
            }, function (data) {
                jQuery('#success-message span').text(data);
                jQuery('#success-message').show();
            });
            // jQuery.post(url, {'form': 'stocazzo'});
            // jQuery.ajax({
            //     type: 'POST',
            //     url: ajaxurl,
            //     data: {
            //         'action': 'thepassenger_create_new_form',
            //         'form': JSON.stringify(this.data())
            //     },
            //     success: function(data) {
            //         jQuery('#success-message').show();
            //     }
            // });
        }
    }, {
        key: 'moveFields',
        value: function moveFields(newOrder) {
            var _this2 = this;

            var newFields = [];
            newOrder.forEach(function (number, index) {
                newFields[index] = _this2.fields[parseInt(number)];
            });
            this.fields = newFields;
        }
    }]);

    return Form;
}();

;

//Props:
//  fields: []
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 
// 

exports.default = Form;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Button = function () {
    function Button() {
        _classCallCheck(this, Button);

        this.type = 'submit';
        this.tag = 'button';
        this.attrs = {
            id: '',
            class: ''
        };
        this.extras = {
            text: ''
        };
    }

    _createClass(Button, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Button;
}();

exports.default = Button;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Checkbox = function () {
    function Checkbox() {
        _classCallCheck(this, Checkbox);

        this.type = 'checkbox';
        this.tag = 'input';
        this.attrs = {
            name: '',
            value: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: '',
            text: ''
        };
    }

    _createClass(Checkbox, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Checkbox;
}();

exports.default = Checkbox;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Color = function () {
    function Color() {
        _classCallCheck(this, Color);

        this.type = 'color';
        this.tag = 'input';
        this.attrs = {
            name: '',
            id: '',
            class: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Color, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Color;
}();

exports.default = Color;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Date = function () {
    function Date() {
        _classCallCheck(this, Date);

        this.type = 'date';
        this.tag = 'input';
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Date, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Date;
}();

exports.default = Date;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DatetimeLocal = function () {
    function DatetimeLocal() {
        _classCallCheck(this, DatetimeLocal);

        this.type = 'datetime-local';
        this.tag = 'input';
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(DatetimeLocal, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return DatetimeLocal;
}();

exports.default = DatetimeLocal;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Email = function () {
    function Email() {
        _classCallCheck(this, Email);

        this.type = 'email';
        this.tag = 'input';
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Email, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Email;
}();

exports.default = Email;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Hidden = function () {
    function Hidden() {
        _classCallCheck(this, Hidden);

        this.type = 'hidden';
        this.tag = 'input';
        this.attrs = {
            name: '',
            value: ''
        };
        this.extras = {};
    }

    _createClass(Hidden, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Hidden;
}();

exports.default = Hidden;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Month = function () {
    function Month() {
        _classCallCheck(this, Month);

        this.type = 'month';
        this.tag = 'input';
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Month, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Month;
}();

exports.default = Month;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Number = function () {
    function Number() {
        _classCallCheck(this, Number);

        this.type = 'number';
        this.tag = 'input';
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false,
            min: '',
            max: ''
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Number, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Number;
}();

exports.default = Number;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Password = function () {
    function Password() {
        _classCallCheck(this, Password);

        this.type = 'password';
        this.tag = 'input';
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Password, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Password;
}();

exports.default = Password;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Radio = function () {
    function Radio() {
        _classCallCheck(this, Radio);

        this.type = 'radio';
        this.tag = 'input';
        this.attrs = {
            name: '',
            value: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: '',
            text: ''
        };
    }

    _createClass(Radio, [{
        key: 'update',
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: 'data',
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Radio;
}();

exports.default = Radio;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Text = function () {
    function Text() {
        _classCallCheck(this, Text);

        this.type = "text";
        this.tag = "input";
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Text, [{
        key: "update",
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: "data",
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Text;
}();

exports.default = Text;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TextArea = function () {
    function TextArea() {
        _classCallCheck(this, TextArea);

        this.type = "";
        this.tag = "textarea";
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false,
            rows: '',
            cols: ''
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(TextArea, [{
        key: "update",
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: "data",
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return TextArea;
}();

exports.default = TextArea;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Url = function () {
    function Url() {
        _classCallCheck(this, Url);

        this.type = "url";
        this.tag = "input";
        this.attrs = {
            name: '',
            id: '',
            class: '',
            placeholder: '',
            required: false
        };
        this.extras = {
            label: ''
        };
    }

    _createClass(Url, [{
        key: "update",
        value: function update(data) {
            var _this = this;

            Object.keys(data.attrs).forEach(function (attr) {
                _this.attrs[attr] = data.attrs[attr];
            });

            Object.keys(data.extras).forEach(function (extra) {
                _this.extras[extra] = data.extras[extra];
            });

            return this;
        }
    }, {
        key: "data",
        value: function data() {
            return {
                type: this.type,
                tag: this.tag,
                attrs: this.attrs,
                extras: this.extras
            };
        }
    }]);

    return Url;
}();

exports.default = Url;

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

exports.default = capitalize;

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Field = __webpack_require__(0);

var _Field2 = _interopRequireDefault(_Field);

var _Form = __webpack_require__(2);

var _Form2 = _interopRequireDefault(_Form);

var _LoadGenerators = __webpack_require__(1);

var _LoadGenerators2 = _interopRequireDefault(_LoadGenerators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

jQuery(document).ready(function ($) {

    $('#success-message').hide();
    // class Test {
    //     constructor(){
    //         this.a = 'asd';
    //         this.b = 'asd';
    //     }
    // }
    // var test = new Test();
    // var test2 = new Test();
    // console.log(test === test2);
    // console.log(test);
    var field = null;

    var formStructure = $('#form-structure').val();
    var form = new _Form2.default(formStructure ? JSON.parse(formStructure) : {
        name: '',
        fields: []
    });

    form.show();
    // Get the modal
    var modal = $('#myModal');
    // dataType = [];
    // When the user clicks on the button, open the modal 

    $('.generator').click(function (event) {
        var fieldType = $(event.target).attr('data-type');
        field = new _Field2.default((0, _LoadGenerators2.default)(fieldType).data());
        field.show();
    });

    // $(document).on('click', '.remove-field', function(event){
    //     console.log(form);

    // });

    // When the user clicks anywhere outside of the modal, close it
    $(window).on('click', function (event) {
        if (event.target == modal[0]) {
            modal.hide();
            $('.modal-content').empty();
        }
    });

    $(document).on('click', '#add-field-button', createField);

    // $('#text-generator').click(function(){
    //     addTextField(); // Generates the field json.
    // });

    // function addTextField() {
    //     $('.modal-content').append(field.show());
    // }

    function createField() {
        field.save();
        form.addField(field);
        form.show();
        $('.modal-content').empty();
        modal.hide();
    }

    $('#create-form-button').on('click', function (event) {
        form.name = $('input[name="thepassenger_form_new_name"]').val();
        form.submit(thepassenger_action.action);
    });

    $(".fields").sortable({
        revert: true,
        update: function update(event, ui) {
            var currentOrder = $(this).sortable('toArray');
            form.moveFields(currentOrder);
        }
    });
});

// {
//     type: 'text',
//     tag: 'input'
//     attrs: [
//         id: 'myId',
//         class: 'myClass',
//         placeholder: 'type ur name here',
//         required: true
//     ],
//     extra: [
//         label: 'My label',

//     ]
// }

/***/ })
/******/ ]);